abstract class SettingsKeys {
  static const String cardsPerSessionKey = 'cards_per_session';
  static const String enableTextToSpeechKey = 'enable_text_to_speach';
  static const String textToSpeechLanguageKey = 'text_to_speech_language';
  static const int defaultCardsPerSessionKey = 10;
}
